addEventListener('fetch', event => {
    event.respondWith(handleRequest(event.request))
})

/**
 * Respond to the request
 * @param {Request} request
 */
async function handleRequest(request) {
    // const username = "seblz432";
    // split access token into the right parts with the right encoding

    //TODO clean this up
    const accessToken = getCookie(request, "accessToken");

    const username = JSON.parse(atob(accessToken.split('.')[1])).sub;
    const partialToken = accessToken.split('.').slice(0, 2).join('.');
    let signaturePart = accessToken.split('.')[2];
    signaturePart = signaturePart.replace(/-/g, "+");
    signaturePart = signaturePart.replace(/_/g, "/");

    // throw error if the access token is invalid

    if (!await verify(str2ab(partialToken), str2ab(atob(signaturePart))))
        return new Response("", {status: 401})
    
    const date = await request.headers.get("date");
    const time = await request.headers.get("time");
    const uuid = await request.headers.get("uuid");


    return new Response(username, {status: 200})
    // return new Response(await getFile(username, date, time, uuid), {status: 200});
}

async function getFile (username, date, time, uuid) {
    const apiURL = "https://f000.backblazeb2.com/file/sepia-libraries";
    const url = apiURL + "/" + username + "/" + date + "/" + time + "_" + uuid;
    const authorizationToken = await STORAGE_ACCESS.get("authorizationToken");

    let headers = new Headers();
    headers.append('Authorization', authorizationToken);

    var requestOptions = {
        method: 'GET',
        headers: headers,
    };

    return await fetch(url, requestOptions);

    /*const response = await fetch(url, requestOptions).then(response => {
        return response;
    })

    const responseParsed = await response.text().then(res => {
        return JSON.parse(res);
    })*/
}

/**
 * Everything below this is used to verify the access token
 * it uses the built in Crypto API to verify a JWT
 */

function getCookie(request, name) {
    let result = ""
    const cookieString = request.headers.get("Cookie")
    if (cookieString) {
        const cookies = cookieString.split(";")
        cookies.forEach(cookie => {
            const cookiePair = cookie.split("=", 2)
            const cookieName = cookiePair[0].trim()
            if (cookieName === name) {
            const cookieVal = cookiePair[1]
            result = cookieVal
            }
        })
    }
    return result
}

async function verify (encoded, signature) {
    //PUBLIC_KEY is set as an environment variable
    const keyObject = await importPublicKey(PUBLIC_KEY);

    return await crypto.subtle.verify(
        {
            name: "ECDSA",
            hash: {name: "SHA-256"},
        },
        keyObject,
        signature,
        encoded
    ).catch((error) => {
        console.log(error);
    });
}

function str2ab(str) {
    const buf = new ArrayBuffer(str.length);
    const bufView = new Uint8Array(buf);
    for (let i = 0, strLen = str.length; i < strLen; i++) {
        bufView[i] = str.charCodeAt(i);
    }
    return buf;
}

async function importPublicKey(pem) {
    // fetch the part of the PEM string between header and footer
    const pemHeader = "-----BEGIN PUBLIC KEY-----";
    const pemFooter = "-----END PUBLIC KEY-----";
    const pemContents = pem.substring(pemHeader.length, pem.length - pemFooter.length);
    // base64 decode the string to get the binary data
    const binaryDerString = atob(pemContents);
    // convert from a binary string to an ArrayBuffer
    const binaryDer = str2ab(binaryDerString);

    return await crypto.subtle.importKey(
        "spki",
        binaryDer,
        {
            name: "ECDSA",
            hash: {name: "SHA-256"},
            namedCurve: "P-256"
        },
        false,
        ["verify"]
    );
}
